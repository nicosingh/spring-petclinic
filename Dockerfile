# stage: jar builder
FROM openjdk:8 as builder

# copy source code into app server
WORKDIR /usr/local/src/spring-petclinic
COPY . .

# build jar package
RUN chmod +x mvnw
RUN ./mvnw package

# stage: app server
FROM tomcat:8

# get jar package from project sources
COPY --from=builder /usr/local/src/spring-petclinic/target/petclinic.war /usr/local/tomcat/webapps
